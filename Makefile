all: egg-fmt

WARNINGS = -Wall -Werror -Wold-style-definition -Wdeclaration-after-statement \
	-Wredundant-decls -Wmissing-noreturn -Wshadow -Wcast-align -Wwrite-strings \
	-Winline -Wformat-nonliteral -Wformat-security -Wswitch-enum -Wswitch-default \
	-Winit-self -Wmissing-include-dirs -Wundef -Waggregate-return \
	-Wmissing-format-attribute -Wnested-externs

FILES = main.c egg-fmt.h egg-fmt.c

egg-fmt: $(FILES)
	$(CC) -g -o $@ $(WARNINGS) $(FILES) `pkg-config --libs --cflags gobject-2.0`

clean:
	rm -rf egg-fmt

