#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gi18n.h>

#include "egg-fmt.h"

static gboolean opt_csv  = FALSE;
static gboolean opt_html = FALSE;

static GOptionEntry entries[] = {
	{ "html", '\0', 0, G_OPTION_ARG_NONE, &opt_html, N_("Output in HTML Table format"), NULL },
	{ "csv", '\0', 0, G_OPTION_ARG_NONE, &opt_csv, N_("Output in CSV format"), NULL },
	{ NULL }
};

static gboolean
parse_int (const gchar *str,
           gint        *v_int)
{
	errno = 0;
	*v_int = strtol (str, NULL, 0);
	return (errno == 0);
}

static gboolean
is_int_type (GType type)
{
	return (type == G_TYPE_INT)  ||
	       (type == G_TYPE_UINT) ||
	       (type == G_TYPE_LONG) ||
	       (type == G_TYPE_ULONG);
}

static gboolean
iter_func (EggFmtIter *iter,
           gpointer    user_data)
{
	GIOChannel  *channel = user_data;
	gchar       *line    = NULL;
	gchar      **parts;
	gboolean     result  = FALSE;
	gint         i, v_int = 0;
	GType        vtype;

	if (G_IO_STATUS_NORMAL != g_io_channel_read_line (channel, &line, NULL, NULL, NULL))
		return FALSE;

	g_strstrip (line);
	parts = g_strsplit (line, "|", iter->n_columns);

	if (g_strv_length (parts) != iter->n_columns) {
		result = FALSE;
		goto cleanup;
	}

	for (i = 0; i < iter->n_columns; i++) {
		vtype = G_VALUE_TYPE (&iter->column_values [i]);

		if (is_int_type (vtype)) {
			parse_int (parts [i], &v_int);
			g_value_set_int (&iter->column_values [i], v_int);
		}
		else {
			g_value_set_string (&iter->column_values [i], parts [i]);
		}
	}

	result = TRUE;

cleanup:
	g_strfreev (parts);
	g_free (line);

	return result;
}

gint
main (gint   argc,
      gchar *argv[])
{
	GOptionContext *context;
	GIOChannel     *channel;
	GError         *error = NULL;
	EggFmtFunc      formatter;
	EggFmtIter      iter;

	/* Parse command line options for formatter */
	context = g_option_context_new ("[FILENAME] - egg-fmt example");
	g_option_context_add_main_entries (context, entries, NULL);
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_printerr ("%s\n", error->message);
		return EXIT_FAILURE;
	}

	/* make sure a filename was provided */
	if (argc < 2) {
		g_printerr ("%s", g_option_context_get_help (context, TRUE, NULL));
		return EXIT_FAILURE;
	}

	g_type_init ();

	/* we will read in a simple pipe delimited file for testing
	 * our data set.  It's format/types are known.
	 */
	if (!(channel = g_io_channel_new_file (argv [1], "r", &error))) {
		g_printerr ("Could not open file %s\n", argv [1]);
		return EXIT_FAILURE;
	}

	/* Set the proper output formatter */
	formatter = opt_csv ? egg_fmt_csv : (opt_html ? egg_fmt_html_table : egg_fmt_table);

	/* initialize the EggFmtIter for use */
	egg_fmt_iter_init (&iter,
	                   iter_func,
	                   "ID",        G_TYPE_INT,
	                   "State",     G_TYPE_STRING,
	                   "Sources",   G_TYPE_INT,
	                   "Program",   G_TYPE_STRING,
	                   "Arguments", G_TYPE_STRING,
	                   NULL);

	/* Write output to console */
	formatter (&iter, channel, NULL);

	return EXIT_SUCCESS;
}
